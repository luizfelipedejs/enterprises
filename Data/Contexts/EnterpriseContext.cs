﻿using Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Data.Contexts
{
    public class EnterpriseContext : IdentityDbContext, IDesignTimeDbContextFactory<EnterpriseContext>
    {
        public EnterpriseContext() { }
        public EnterpriseContext(DbContextOptions<EnterpriseContext> options) : base(options) { }

        public EnterpriseContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"{Directory.GetCurrentDirectory()}/../Api/appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<EnterpriseContext>();
            var connectionString = configuration.GetConnectionString("EnterpriseContext");

            builder.UseSqlServer(connectionString);

            return new EnterpriseContext(builder.Options);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(EnterpriseContext).Assembly);

            builder.InsertIndentityUsers();
            builder.InsertInvestors();
            builder.InsertEnterpriseTypes();
            builder.InsertEnterprises();

            base.OnModelCreating(builder);
        }

        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<EnterpriseType> EnterpriseTypes { get; set; }
        public DbSet<Investor> Investors { get; set; }
    }
}
