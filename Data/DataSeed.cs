﻿using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public static class DataSeed
    {
        public static void InsertIndentityUsers(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUser>()
                .HasData(new IdentityUser()
                {
                    UserName = "testeapple@ioasys.com.br",
                    NormalizedUserName = "TESTEAPPLE@IOASYS.COM.BR",
                    Email = "testeapple@ioasys.com.br",
                    NormalizedEmail = "TESTEAPPLE@IOASYS.COM.BR",
                    EmailConfirmed = true,
                    PasswordHash = "AQAAAAEAACcQAAAAEDIvBoKK66FUdodmD5jX1EEdbd5Osn4SWdzFEh+lpmgvaMiWU1SrmDsOhqcd3nB18g==",
                    SecurityStamp = "UX2NCKY3CEF2MO4WINEBWVVUQIXKD2A2"
                });
        }

        public static void InsertInvestors(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Investor>()
                .HasData(new Investor()
                {
                    Id = 1,
                    InvestorName = "Teste Apple",
                    Email = "testeapple@ioasys.com.br",
                    City = "BH",
                    Country = "Brasil",
                    Balance = 350000.0m,
                    Photo = "/uploads/investor/photo/1/cropped4991818370070749122.jpg",
                    PortfolioValue = 350000.0m,
                    FirstAccess = false,
                    SuperAngel = false
                });
        }

        public static void InsertEnterpriseTypes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EnterpriseType>()
                .HasData(
                new EnterpriseType()
                {
                    Id = 1,
                    EnterpriseTypeName = "Agro"
                },
                new EnterpriseType()
                {
                    Id = 2,
                    EnterpriseTypeName = "Aviation"
                },
                new EnterpriseType()
                {
                    Id = 3,
                    EnterpriseTypeName = "Biotech"
                },
                new EnterpriseType()
                {
                    Id = 4,
                    EnterpriseTypeName = "Eco"
                },
                new EnterpriseType()
                {
                    Id = 5,
                    EnterpriseTypeName = "Ecommerce"
                },
                new EnterpriseType()
                {
                    Id = 6,
                    EnterpriseTypeName = "Education"
                },
                new EnterpriseType()
                {
                    Id = 7,
                    EnterpriseTypeName = "Fashion"
                },
                new EnterpriseType()
                {
                    Id = 8,
                    EnterpriseTypeName = "Fintech"
                },
                new EnterpriseType()
                {
                    Id = 9,
                    EnterpriseTypeName = "Food"
                },
                new EnterpriseType()
                {
                    Id = 10,
                    EnterpriseTypeName = "Games"
                },
                new EnterpriseType()
                {
                    Id = 11,
                    EnterpriseTypeName = "Health"
                },
                new EnterpriseType()
                {
                    Id = 12,
                    EnterpriseTypeName = "IOT"
                },
                new EnterpriseType()
                {
                    Id = 13,
                    EnterpriseTypeName = "Logistics"
                },
                new EnterpriseType()
                {
                    Id = 14,
                    EnterpriseTypeName = "Media"
                },
                new EnterpriseType()
                {
                    Id = 15,
                    EnterpriseTypeName = "Mining"
                },
                new EnterpriseType()
                {
                    Id = 16,
                    EnterpriseTypeName = "Products"
                },
                new EnterpriseType()
                {
                    Id = 17,
                    EnterpriseTypeName = "Real Estate"
                },
                new EnterpriseType()
                {
                    Id = 18,
                    EnterpriseTypeName = "Service"
                },
                new EnterpriseType()
                {
                    Id = 19,
                    EnterpriseTypeName = "Smart City"
                },
                new EnterpriseType()
                {
                    Id = 20,
                    EnterpriseTypeName = "Social"
                },
                new EnterpriseType()
                {
                    Id = 21,
                    EnterpriseTypeName = "Software"
                },
                new EnterpriseType()
                {
                    Id = 22,
                    EnterpriseTypeName = "Technology"
                },
                new EnterpriseType()
                {
                    Id = 23,
                    EnterpriseTypeName = "Tourism"
                },
                new EnterpriseType()
                {
                    Id = 24,
                    EnterpriseTypeName = "Transport"
                },
                new EnterpriseType()
                {
                    Id = 25,
                    EnterpriseTypeName = "Arts"
                },
                new EnterpriseType()
                {
                    Id = 26,
                    EnterpriseTypeName = "C2C"
                },
                new EnterpriseType()
                {
                    Id = 27,
                    EnterpriseTypeName = "Digital Media"
                });
        }

        public static void InsertEnterprises(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Enterprise>()
                .HasData(
                new Enterprise()
                {
                    Id = 1,
                    EmailEnterprise = "",
                    Facebook = "",
                    Twitter = "",
                    Linkedin = "",
                    Phone = "",
                    OwnEnterprise = false,
                    EnterpriseName = "AllRide",
                    Photo = "/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg",
                    Description = "Urbanatika is a socio-environmental company with economic impact, creator of the " +
                        "agro-urban industry. We want to involve people in the processes of healthy eating, recycling " +
                        "and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter " +
                        "cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry",
                    City = "Santiago",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 21
                },
                new Enterprise()
                {
                    Id = 2,
                    EmailEnterprise = "",
                    Facebook = "",
                    Twitter = "",
                    Linkedin = "",
                    Phone = "",
                    OwnEnterprise = false,
                    EnterpriseName = "Alpaca Samka SpA",
                    Photo = "/uploads/enterprise/photo/2/WhatsApp_Image_2017-10-31_at_13.47.22.jpeg",
                    Description = "Alpaca Samka uses alpaca fibres for our “Slow Fashion Project” " +
                        "in association with the Aymaras of the Chilean Andes, producing sustainable luxury " +
                        "accessories and garments using traditional Andean methods and British weaving patterns. " +
                        "We are part of the Inward Investment Program and have been recognised by international organisations. ",
                    City = "Viña del Mar",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 7
                },
                new Enterprise()
                {
                    Id = 3,
                    EmailEnterprise = "",
                    Facebook = "",
                    Twitter = "",
                    Linkedin = "",
                    Phone = "",
                    OwnEnterprise = false,
                    EnterpriseName = "AnewLytics SpA",
                    Photo = "/uploads/enterprise/photo/3/thumb_8016a7d8a952351f3cb4d5f485f43ea1.octet-stream",
                    Description = " We have one passion: to create value for our customers by analyzing the " +
                        "conversations their customers have with the Contact Center in order to extract valuable " +
                        "and timely information to understand and meet their needs. That´s how AnewLytics was born: " +
                        "a cloud-based analytics service platform that performs 100% automated analysis.",
                    City = "Santiago",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 18
                },
                new Enterprise()
                {
                    Id = 4,
                    EmailEnterprise = null,
                    Facebook = null,
                    Twitter = null,
                    Linkedin = null,
                    Phone = null,
                    OwnEnterprise = false,
                    EnterpriseName = "AQM S.A.",
                    Photo = null,
                    Description = "Cold Killer was discovered by chance in the ´90 s and developed by Mrs. Inés " +
                        "Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, " +
                        "AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) " +
                        "up to 30% therefore help plants resists cold weather. ",
                    City = "Maule",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 1
                },
                new Enterprise()
                {
                    Id = 5,
                    EmailEnterprise = null,
                    Facebook = null,
                    Twitter = null,
                    Linkedin = null,
                    Phone = null,
                    OwnEnterprise = false,
                    EnterpriseName = "Árbol Sabores",
                    Photo = null,
                    Description = "We are Arbol Sabores, a new generation of healthy food that has a positive impact in the " +
                        "environment and society. We want to change the world through the feeding behaviors of the society, " +
                        "giving seeds of urban orchards in ours products and by innovating with biodegradable packing.",
                    City = "Santiago",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 11
                },
                new Enterprise()
                {
                    Id = 6,
                    EmailEnterprise = null,
                    Facebook = null,
                    Twitter = null,
                    Linkedin = null,
                    Phone = null,
                    OwnEnterprise = false,
                    EnterpriseName = "ArchDaily",
                    Photo = null,
                    Description = "Our mission is to improve the quality of life of the next 3 billion people living in " +
                        "cities by 2050, providing inspiration, knowledge, and tools to the architects who fill face this challenge.",
                    City = "Santiago",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 14
                },
                new Enterprise()
                {
                    Id = 7,
                    EmailEnterprise = null,
                    Facebook = null,
                    Twitter = null,
                    Linkedin = null,
                    Phone = null,
                    OwnEnterprise = false,
                    EnterpriseName = "Aveeza",
                    Photo = null,
                    Description = "Aveeza is an intelligent platform specially developed for managing school transportation, anywhere. " +
                        "From real time ridership status, route optimisation, fleet maintenance to driver management, Aveeza brings world " +
                        "class logistics to school transportation, so the world’s most precious cargo, children, can be monitored in full " +
                        "transparency that the industry finally deserves.",
                    City = "Providencia",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 24
                },
                new Enterprise()
                {
                    Id = 8,
                    EmailEnterprise = null,
                    Facebook = null,
                    Twitter = null,
                    Linkedin = null,
                    Phone = null,
                    OwnEnterprise = false,
                    EnterpriseName = "BREAL ESTATE SPA",
                    Photo = null,
                    Description = "Breal Estate is a Chilean company established in 2013, which, in a strategic alliance with Salesforce.com, " +
                        "initially developed an application for property management. Currently, BReal is an application that incorporates all " +
                        "the functions necessary to manage different processes of the real estate business: property management , sales, leases, " +
                        "common expenses and projects with the best practices in mind. It is delivered as a service (SaaS) and accessed via " +
                        "Internet from any device.",
                    City = "SANTIAGO",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 17
                },
                new Enterprise()
                {
                    Id = 9,
                    EmailEnterprise = "",
                    Facebook = "",
                    Twitter = "",
                    Linkedin = "",
                    Phone = "",
                    OwnEnterprise = false,
                    EnterpriseName = "Capitalizarme.com",
                    Photo = "/uploads/enterprise/photo/9/cruzeiro.png",
                    Description = "We are a intermediary between developers in real estate and small investors who want to get good investment opportunities.  ",
                    City = "Santiago",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 17
                },
                new Enterprise()
                {
                    Id = 10,
                    EmailEnterprise = null,
                    Facebook = null,
                    Twitter = null,
                    Linkedin = null,
                    Phone = null,
                    OwnEnterprise = false,
                    EnterpriseName = "Capta Hydro",
                    Photo = null,
                    Description = "Capta Hydro is a clean energy technology startup that develops and commercialises hydropower technology for generation in " +
                        "artificial canals without the need of falls, that are economically competitive to other distributed generation alternatives and electric " +
                        "utility tariffs. Our BHAG is to install 1 GW of our technology  by 2027.",
                    City = "Santiago",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 4
                },
                new Enterprise()
                {
                    Id = 11,
                    EmailEnterprise = null,
                    Facebook = null,
                    Twitter = null,
                    Linkedin = null,
                    Phone = null,
                    OwnEnterprise = false,
                    EnterpriseName = "Ceptinel",
                    Photo = null,
                    Description = "Our product, Ceptinel Risk Manager, is a real-time business procecess monitoring system that, with the use of " +
                        "applied business rules algorithms, detects and notifies when potentially harmful or beneficial situations are detected.",
                    City = "Santiago",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 8
                },
                new Enterprise()
                {
                    Id = 12,
                    EmailEnterprise = null,
                    Facebook = null,
                    Twitter = null,
                    Linkedin = null,
                    Phone = null,
                    OwnEnterprise = false,
                    EnterpriseName = "CGS",
                    Photo = null,
                    Description = "Beyond Productivity: RMES is an asset Performance Management Software that helps capital intensive companies " +
                        "increase productivity through a better use of assets. This technology gets big ammount of data from equipment operation " +
                        "and failures and analyze it to identify improvement opportunities using complex algorithms.",
                    City = "Santiago",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 21
                },
                new Enterprise()
                {
                    Id = 13,
                    EmailEnterprise = null,
                    Facebook = null,
                    Twitter = null,
                    Linkedin = null,
                    Phone = null,
                    OwnEnterprise = false,
                    EnterpriseName = "ClicEduca",
                    Photo = null,
                    Description = "ClicEduca - Intesis is a technology development company for education.  Our multidisciplinary team works on " +
                        "RESEARCH AND DEVELOPMENT which has resulted in technological innovations available in educational institutions such as " +
                        "ClicEduca.com cloud and our star product, MusíGlota.  Our know-how philosophy is our base and driving force to develop " +
                        "high educational technology, enhancing teaching and learning processes with innovative solutions for teachers, " +
                        "students, administrators and family today. ",
                    City = "Santiago",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 22
                },
                new Enterprise()
                {
                    Id = 14,
                    EmailEnterprise = null,
                    Facebook = null,
                    Twitter = null,
                    Linkedin = null,
                    Phone = null,
                    OwnEnterprise = false,
                    EnterpriseName = "ClicEduca",
                    Photo = null,
                    Description = "ClicEduca - Intesis is a technology development company for education.  Our multidisciplinary team works on " +
                        "RESEARCH AND DEVELOPMENT which has resulted in technological innovations available in educational institutions such as ClicEduca.com " +
                        "cloud and our star product, MusíGlota.  Our know-how philosophy is our base and driving force to develop high educational technology, " +
                        "enhancing teaching and learning processes with innovative solutions for teachers, students, administrators and family today. ",
                    City = "Santiago",
                    Country = "Chile",
                    Value = 0,
                    SharePrice = 5000.0m,
                    EnterpriseTypeId = 22
                });
        }
    }
}
