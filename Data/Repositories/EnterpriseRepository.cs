﻿using Data.Contexts;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class EnterpriseRepository : IEnterpriseRepository
    {
        private readonly EnterpriseContext _enterpriseContext;

        public EnterpriseRepository(EnterpriseContext enterpriseContext)
        {
            _enterpriseContext = enterpriseContext;
        }

        public async Task<Enterprise> GetEnterprise(int id) =>
                await _enterpriseContext.Enterprises
                .Include(d => d.EnterpriseType)
                .AsNoTracking()
                .FirstOrDefaultAsync(d => d.Id == id);

        public async Task<IEnumerable<Enterprise>> GetEnterprises(
            params Expression<Func<Enterprise, bool>>[] whereFilters)
        {
            var enterprises = _enterpriseContext.Enterprises
                .Include(d => d.EnterpriseType)
                .AsNoTracking();
            foreach (var whereFilter in whereFilters)
                enterprises = enterprises.Where(whereFilter);

            return await enterprises.ToListAsync();
        }

    }
}
