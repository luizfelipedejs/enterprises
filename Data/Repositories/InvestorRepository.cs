﻿using Data.Contexts;
using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class InvestorRepository : IInvestorRepository
    {
        private readonly EnterpriseContext _enterpriseContext;

        public InvestorRepository(EnterpriseContext enterpriseContext)
        {
            _enterpriseContext = enterpriseContext;
        }

        public async Task<Investor> GetInvestor(string email) =>
            await _enterpriseContext.Investors
            .Include(d => d.Enterprises)
            .ThenInclude(enterprise => enterprise.EnterpriseType)
            .AsNoTracking()
            .FirstOrDefaultAsync(d => d.Email == email);

        public IQueryable<Investor> GetInvestors() =>
            _enterpriseContext.Investors
            .Include(d => d.Enterprises)
            .ThenInclude(enterprise => enterprise.EnterpriseType)
            .AsNoTracking();
    }
}
