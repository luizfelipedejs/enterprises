﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EnterpriseType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EnterpriseTypeName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnterpriseType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Investor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InvestorName = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    Country = table.Column<string>(nullable: false),
                    Balance = table.Column<decimal>(nullable: false),
                    Photo = table.Column<string>(nullable: true),
                    PortfolioValue = table.Column<decimal>(nullable: false),
                    FirstAccess = table.Column<bool>(nullable: false),
                    SuperAngel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Investor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Enterprise",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EnterpriseName = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: false),
                    EmailEnterprise = table.Column<string>(nullable: true),
                    Facebook = table.Column<string>(nullable: true),
                    Twitter = table.Column<string>(nullable: true),
                    Linkedin = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    OwnEnterprise = table.Column<bool>(nullable: false, defaultValue: false),
                    Photo = table.Column<string>(nullable: true),
                    Value = table.Column<int>(nullable: false),
                    Shares = table.Column<int>(nullable: false),
                    SharePrice = table.Column<decimal>(nullable: false),
                    OwnShares = table.Column<int>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    EnterpriseTypeId = table.Column<int>(nullable: false),
                    InvestorId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enterprise", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enterprise_EnterpriseType_EnterpriseTypeId",
                        column: x => x.EnterpriseTypeId,
                        principalTable: "EnterpriseType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Enterprise_Investor_InvestorId",
                        column: x => x.InvestorId,
                        principalTable: "Investor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "fac63e87-351d-416d-ae81-cb7b6b61f7b1", 0, "2cca374b-485e-4ae9-8662-e49d9f91ce10", "testeapple@ioasys.com.br", true, false, null, "TESTEAPPLE@IOASYS.COM.BR", "TESTEAPPLE@IOASYS.COM.BR", "AQAAAAEAACcQAAAAEDIvBoKK66FUdodmD5jX1EEdbd5Osn4SWdzFEh+lpmgvaMiWU1SrmDsOhqcd3nB18g==", null, false, "UX2NCKY3CEF2MO4WINEBWVVUQIXKD2A2", false, "testeapple@ioasys.com.br" });

            migrationBuilder.InsertData(
                table: "EnterpriseType",
                columns: new[] { "Id", "EnterpriseTypeName" },
                values: new object[,]
                {
                    { 27, "Digital Media" },
                    { 26, "C2C" },
                    { 25, "Arts" },
                    { 24, "Transport" },
                    { 23, "Tourism" },
                    { 22, "Technology" },
                    { 21, "Software" },
                    { 20, "Social" },
                    { 19, "Smart City" },
                    { 18, "Service" },
                    { 17, "Real Estate" },
                    { 16, "Products" },
                    { 15, "Mining" },
                    { 13, "Logistics" },
                    { 12, "IOT" },
                    { 11, "Health" },
                    { 10, "Games" },
                    { 9, "Food" },
                    { 8, "Fintech" },
                    { 7, "Fashion" },
                    { 6, "Education" },
                    { 5, "Ecommerce" },
                    { 4, "Eco" },
                    { 3, "Biotech" },
                    { 2, "Aviation" },
                    { 14, "Media" },
                    { 1, "Agro" }
                });

            migrationBuilder.InsertData(
                table: "Investor",
                columns: new[] { "Id", "Balance", "City", "Country", "Email", "FirstAccess", "InvestorName", "Photo", "PortfolioValue", "SuperAngel" },
                values: new object[] { 1, 350000.0m, "BH", "Brasil", "testeapple@ioasys.com.br", false, "Teste Apple", "/uploads/investor/photo/1/cropped4991818370070749122.jpg", 350000.0m, false });

            migrationBuilder.InsertData(
                table: "Enterprise",
                columns: new[] { "Id", "City", "Country", "Description", "EmailEnterprise", "EnterpriseName", "EnterpriseTypeId", "Facebook", "InvestorId", "Linkedin", "OwnShares", "Phone", "Photo", "SharePrice", "Shares", "Twitter", "Value" },
                values: new object[,]
                {
                    { 4, "Maule", "Chile", "Cold Killer was discovered by chance in the ´90 s and developed by Mrs. Inés Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ", null, "AQM S.A.", 1, null, null, null, 0, null, null, 5000.0m, 0, null, 0 },
                    { 10, "Santiago", "Chile", "Capta Hydro is a clean energy technology startup that develops and commercialises hydropower technology for generation in artificial canals without the need of falls, that are economically competitive to other distributed generation alternatives and electric utility tariffs. Our BHAG is to install 1 GW of our technology  by 2027.", null, "Capta Hydro", 4, null, null, null, 0, null, null, 5000.0m, 0, null, 0 },
                    { 2, "Viña del Mar", "Chile", "Alpaca Samka uses alpaca fibres for our “Slow Fashion Project” in association with the Aymaras of the Chilean Andes, producing sustainable luxury accessories and garments using traditional Andean methods and British weaving patterns. We are part of the Inward Investment Program and have been recognised by international organisations. ", "", "Alpaca Samka SpA", 7, "", null, "", 0, "", "/uploads/enterprise/photo/2/WhatsApp_Image_2017-10-31_at_13.47.22.jpeg", 5000.0m, 0, "", 0 },
                    { 11, "Santiago", "Chile", "Our product, Ceptinel Risk Manager, is a real-time business procecess monitoring system that, with the use of applied business rules algorithms, detects and notifies when potentially harmful or beneficial situations are detected.", null, "Ceptinel", 8, null, null, null, 0, null, null, 5000.0m, 0, null, 0 },
                    { 5, "Santiago", "Chile", "We are Arbol Sabores, a new generation of healthy food that has a positive impact in the environment and society. We want to change the world through the feeding behaviors of the society, giving seeds of urban orchards in ours products and by innovating with biodegradable packing.", null, "Árbol Sabores", 11, null, null, null, 0, null, null, 5000.0m, 0, null, 0 },
                    { 6, "Santiago", "Chile", "Our mission is to improve the quality of life of the next 3 billion people living in cities by 2050, providing inspiration, knowledge, and tools to the architects who fill face this challenge.", null, "ArchDaily", 14, null, null, null, 0, null, null, 5000.0m, 0, null, 0 },
                    { 8, "SANTIAGO", "Chile", "Breal Estate is a Chilean company established in 2013, which, in a strategic alliance with Salesforce.com, initially developed an application for property management. Currently, BReal is an application that incorporates all the functions necessary to manage different processes of the real estate business: property management , sales, leases, common expenses and projects with the best practices in mind. It is delivered as a service (SaaS) and accessed via Internet from any device.", null, "BREAL ESTATE SPA", 17, null, null, null, 0, null, null, 5000.0m, 0, null, 0 },
                    { 9, "Santiago", "Chile", "We are a intermediary between developers in real estate and small investors who want to get good investment opportunities.  ", "", "Capitalizarme.com", 17, "", null, "", 0, "", "/uploads/enterprise/photo/9/cruzeiro.png", 5000.0m, 0, "", 0 },
                    { 3, "Santiago", "Chile", " We have one passion: to create value for our customers by analyzing the conversations their customers have with the Contact Center in order to extract valuable and timely information to understand and meet their needs. That´s how AnewLytics was born: a cloud-based analytics service platform that performs 100% automated analysis.", "", "AnewLytics SpA", 18, "", null, "", 0, "", "/uploads/enterprise/photo/3/thumb_8016a7d8a952351f3cb4d5f485f43ea1.octet-stream", 5000.0m, 0, "", 0 },
                    { 1, "Santiago", "Chile", "Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry", "", "AllRide", 21, "", null, "", 0, "", "/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg", 5000.0m, 0, "", 0 },
                    { 12, "Santiago", "Chile", "Beyond Productivity: RMES is an asset Performance Management Software that helps capital intensive companies increase productivity through a better use of assets. This technology gets big ammount of data from equipment operation and failures and analyze it to identify improvement opportunities using complex algorithms.", null, "CGS", 21, null, null, null, 0, null, null, 5000.0m, 0, null, 0 },
                    { 13, "Santiago", "Chile", "ClicEduca - Intesis is a technology development company for education.  Our multidisciplinary team works on RESEARCH AND DEVELOPMENT which has resulted in technological innovations available in educational institutions such as ClicEduca.com cloud and our star product, MusíGlota.  Our know-how philosophy is our base and driving force to develop high educational technology, enhancing teaching and learning processes with innovative solutions for teachers, students, administrators and family today. ", null, "ClicEduca", 22, null, null, null, 0, null, null, 5000.0m, 0, null, 0 },
                    { 14, "Santiago", "Chile", "ClicEduca - Intesis is a technology development company for education.  Our multidisciplinary team works on RESEARCH AND DEVELOPMENT which has resulted in technological innovations available in educational institutions such as ClicEduca.com cloud and our star product, MusíGlota.  Our know-how philosophy is our base and driving force to develop high educational technology, enhancing teaching and learning processes with innovative solutions for teachers, students, administrators and family today. ", null, "ClicEduca", 22, null, null, null, 0, null, null, 5000.0m, 0, null, 0 },
                    { 7, "Providencia", "Chile", "Aveeza is an intelligent platform specially developed for managing school transportation, anywhere. From real time ridership status, route optimisation, fleet maintenance to driver management, Aveeza brings world class logistics to school transportation, so the world’s most precious cargo, children, can be monitored in full transparency that the industry finally deserves.", null, "Aveeza", 24, null, null, null, 0, null, null, 5000.0m, 0, null, 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Enterprise_EnterpriseTypeId",
                table: "Enterprise",
                column: "EnterpriseTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Enterprise_InvestorId",
                table: "Enterprise",
                column: "InvestorId");

            migrationBuilder.CreateIndex(
                name: "IX_Investor_Email",
                table: "Investor",
                column: "Email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Enterprise");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "EnterpriseType");

            migrationBuilder.DropTable(
                name: "Investor");
        }
    }
}
