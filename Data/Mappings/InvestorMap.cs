﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Mappings
{
    public class InvestorMap : IEntityTypeConfiguration<Investor>
    {
        public void Configure(EntityTypeBuilder<Investor> builder)
        {
            builder.HasKey(d => d.Id);
            builder.Property(d => d.InvestorName)
                .IsRequired();
            builder.Property(d => d.Email)
                .IsRequired();
            builder.HasIndex(d => d.Email)
                .IsUnique();
            builder.Property(d => d.City)
                .IsRequired();
            builder.Property(d => d.Country)
                .IsRequired();
            builder.Property(d => d.Balance)
                .IsRequired();
            builder.Property(d => d.Photo);
            builder.Property(d => d.PortfolioValue);
            builder.Property(d => d.FirstAccess);
            builder.Property(d => d.SuperAngel);

            builder.HasMany(d => d.Enterprises);

            builder.ToTable("Investor");
        }
    }
}
