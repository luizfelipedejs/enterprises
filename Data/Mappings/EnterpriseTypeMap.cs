﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Mappings
{
    public class EnterpriseTypeMap : IEntityTypeConfiguration<EnterpriseType>
    {
        public void Configure(EntityTypeBuilder<EnterpriseType> builder)
        {
            builder.HasKey(d => d.Id);

            builder.Property(d => d.EnterpriseTypeName)
                .IsRequired();

            builder.HasMany<Enterprise>(d => d.Enterprises)
                .WithOne(d => d.EnterpriseType)
                .HasForeignKey(d => d.EnterpriseTypeId);

            builder.ToTable("EnterpriseType");
        }
    }
}
