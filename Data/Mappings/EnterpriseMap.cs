﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Mappings
{
    public class EnterpriseMap : IEntityTypeConfiguration<Enterprise>
    {
        public void Configure(EntityTypeBuilder<Enterprise> builder)
        {
            builder.HasKey(d => d.Id);

            builder.Property(d => d.EnterpriseName)
                .IsRequired();
            builder.Property(d => d.Description)
                .IsRequired();

            builder.Property(d => d.EmailEnterprise);
            builder.Property(d => d.Facebook);
            builder.Property(d => d.Twitter);
            builder.Property(d => d.Linkedin);
            builder.Property(d => d.Phone);
            builder.Property(d => d.OwnEnterprise)
                .HasDefaultValue(false);
            builder.Property(d => d.Photo);
            builder.Property(d => d.Value);
            builder.Property(d => d.Shares);
            builder.Property(d => d.SharePrice);
            builder.Property(d => d.OwnShares);
            builder.Property(d => d.City);
            builder.Property(d => d.Country);

            builder.HasOne(d => d.EnterpriseType)
                .WithMany(d => d.Enterprises);

            builder.ToTable("Enterprise");
        }
    }
}
