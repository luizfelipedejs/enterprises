IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [AspNetRoles] (
    [Id] nvarchar(450) NOT NULL,
    [Name] nvarchar(256) NULL,
    [NormalizedName] nvarchar(256) NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetUsers] (
    [Id] nvarchar(450) NOT NULL,
    [UserName] nvarchar(256) NULL,
    [NormalizedUserName] nvarchar(256) NULL,
    [Email] nvarchar(256) NULL,
    [NormalizedEmail] nvarchar(256) NULL,
    [EmailConfirmed] bit NOT NULL,
    [PasswordHash] nvarchar(max) NULL,
    [SecurityStamp] nvarchar(max) NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    [PhoneNumber] nvarchar(max) NULL,
    [PhoneNumberConfirmed] bit NOT NULL,
    [TwoFactorEnabled] bit NOT NULL,
    [LockoutEnd] datetimeoffset NULL,
    [LockoutEnabled] bit NOT NULL,
    [AccessFailedCount] int NOT NULL,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [EnterpriseType] (
    [Id] int NOT NULL IDENTITY,
    [EnterpriseTypeName] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_EnterpriseType] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Investor] (
    [Id] int NOT NULL IDENTITY,
    [InvestorName] nvarchar(max) NOT NULL,
    [Email] nvarchar(450) NOT NULL,
    [City] nvarchar(max) NOT NULL,
    [Country] nvarchar(max) NOT NULL,
    [Balance] decimal(18,2) NOT NULL,
    [Photo] nvarchar(max) NULL,
    [PortfolioValue] decimal(18,2) NOT NULL,
    [FirstAccess] bit NOT NULL,
    [SuperAngel] bit NOT NULL,
    CONSTRAINT [PK_Investor] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetRoleClaims] (
    [Id] int NOT NULL IDENTITY,
    [RoleId] nvarchar(450) NOT NULL,
    [ClaimType] nvarchar(max) NULL,
    [ClaimValue] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserClaims] (
    [Id] int NOT NULL IDENTITY,
    [UserId] nvarchar(450) NOT NULL,
    [ClaimType] nvarchar(max) NULL,
    [ClaimValue] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserLogins] (
    [LoginProvider] nvarchar(450) NOT NULL,
    [ProviderKey] nvarchar(450) NOT NULL,
    [ProviderDisplayName] nvarchar(max) NULL,
    [UserId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
    CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserRoles] (
    [UserId] nvarchar(450) NOT NULL,
    [RoleId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
    CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserTokens] (
    [UserId] nvarchar(450) NOT NULL,
    [LoginProvider] nvarchar(450) NOT NULL,
    [Name] nvarchar(450) NOT NULL,
    [Value] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
    CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Enterprise] (
    [Id] int NOT NULL IDENTITY,
    [EnterpriseName] nvarchar(max) NOT NULL,
    [Description] nvarchar(max) NOT NULL,
    [EmailEnterprise] nvarchar(max) NULL,
    [Facebook] nvarchar(max) NULL,
    [Twitter] nvarchar(max) NULL,
    [Linkedin] nvarchar(max) NULL,
    [Phone] nvarchar(max) NULL,
    [OwnEnterprise] bit NOT NULL DEFAULT CAST(0 AS bit),
    [Photo] nvarchar(max) NULL,
    [Value] int NOT NULL,
    [Shares] int NOT NULL,
    [SharePrice] decimal(18,2) NOT NULL,
    [OwnShares] int NOT NULL,
    [City] nvarchar(max) NULL,
    [Country] nvarchar(max) NULL,
    [EnterpriseTypeId] int NOT NULL,
    [InvestorId] int NULL,
    CONSTRAINT [PK_Enterprise] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Enterprise_EnterpriseType_EnterpriseTypeId] FOREIGN KEY ([EnterpriseTypeId]) REFERENCES [EnterpriseType] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Enterprise_Investor_InvestorId] FOREIGN KEY ([InvestorId]) REFERENCES [Investor] ([Id]) ON DELETE NO ACTION
);

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AccessFailedCount', N'ConcurrencyStamp', N'Email', N'EmailConfirmed', N'LockoutEnabled', N'LockoutEnd', N'NormalizedEmail', N'NormalizedUserName', N'PasswordHash', N'PhoneNumber', N'PhoneNumberConfirmed', N'SecurityStamp', N'TwoFactorEnabled', N'UserName') AND [object_id] = OBJECT_ID(N'[AspNetUsers]'))
    SET IDENTITY_INSERT [AspNetUsers] ON;
INSERT INTO [AspNetUsers] ([Id], [AccessFailedCount], [ConcurrencyStamp], [Email], [EmailConfirmed], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName])
VALUES (N'fac63e87-351d-416d-ae81-cb7b6b61f7b1', 0, N'2cca374b-485e-4ae9-8662-e49d9f91ce10', N'testeapple@ioasys.com.br', CAST(1 AS bit), CAST(0 AS bit), NULL, N'TESTEAPPLE@IOASYS.COM.BR', N'TESTEAPPLE@IOASYS.COM.BR', N'AQAAAAEAACcQAAAAEDIvBoKK66FUdodmD5jX1EEdbd5Osn4SWdzFEh+lpmgvaMiWU1SrmDsOhqcd3nB18g==', NULL, CAST(0 AS bit), N'UX2NCKY3CEF2MO4WINEBWVVUQIXKD2A2', CAST(0 AS bit), N'testeapple@ioasys.com.br');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AccessFailedCount', N'ConcurrencyStamp', N'Email', N'EmailConfirmed', N'LockoutEnabled', N'LockoutEnd', N'NormalizedEmail', N'NormalizedUserName', N'PasswordHash', N'PhoneNumber', N'PhoneNumberConfirmed', N'SecurityStamp', N'TwoFactorEnabled', N'UserName') AND [object_id] = OBJECT_ID(N'[AspNetUsers]'))
    SET IDENTITY_INSERT [AspNetUsers] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'EnterpriseTypeName') AND [object_id] = OBJECT_ID(N'[EnterpriseType]'))
    SET IDENTITY_INSERT [EnterpriseType] ON;
INSERT INTO [EnterpriseType] ([Id], [EnterpriseTypeName])
VALUES (27, N'Digital Media'),
(26, N'C2C'),
(25, N'Arts'),
(24, N'Transport'),
(23, N'Tourism'),
(22, N'Technology'),
(21, N'Software'),
(20, N'Social'),
(19, N'Smart City'),
(18, N'Service'),
(17, N'Real Estate'),
(16, N'Products'),
(15, N'Mining'),
(13, N'Logistics'),
(12, N'IOT'),
(11, N'Health'),
(10, N'Games'),
(9, N'Food'),
(8, N'Fintech'),
(7, N'Fashion'),
(6, N'Education'),
(5, N'Ecommerce'),
(4, N'Eco'),
(3, N'Biotech'),
(2, N'Aviation'),
(14, N'Media'),
(1, N'Agro');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'EnterpriseTypeName') AND [object_id] = OBJECT_ID(N'[EnterpriseType]'))
    SET IDENTITY_INSERT [EnterpriseType] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Balance', N'City', N'Country', N'Email', N'FirstAccess', N'InvestorName', N'Photo', N'PortfolioValue', N'SuperAngel') AND [object_id] = OBJECT_ID(N'[Investor]'))
    SET IDENTITY_INSERT [Investor] ON;
INSERT INTO [Investor] ([Id], [Balance], [City], [Country], [Email], [FirstAccess], [InvestorName], [Photo], [PortfolioValue], [SuperAngel])
VALUES (1, 350000.0, N'BH', N'Brasil', N'testeapple@ioasys.com.br', CAST(0 AS bit), N'Teste Apple', N'/uploads/investor/photo/1/cropped4991818370070749122.jpg', 350000.0, CAST(0 AS bit));
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Balance', N'City', N'Country', N'Email', N'FirstAccess', N'InvestorName', N'Photo', N'PortfolioValue', N'SuperAngel') AND [object_id] = OBJECT_ID(N'[Investor]'))
    SET IDENTITY_INSERT [Investor] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'City', N'Country', N'Description', N'EmailEnterprise', N'EnterpriseName', N'EnterpriseTypeId', N'Facebook', N'InvestorId', N'Linkedin', N'OwnShares', N'Phone', N'Photo', N'SharePrice', N'Shares', N'Twitter', N'Value') AND [object_id] = OBJECT_ID(N'[Enterprise]'))
    SET IDENTITY_INSERT [Enterprise] ON;
INSERT INTO [Enterprise] ([Id], [City], [Country], [Description], [EmailEnterprise], [EnterpriseName], [EnterpriseTypeId], [Facebook], [InvestorId], [Linkedin], [OwnShares], [Phone], [Photo], [SharePrice], [Shares], [Twitter], [Value])
VALUES (4, N'Maule', N'Chile', N'Cold Killer was discovered by chance in the ┬┤90 s and developed by Mrs. In├®s Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ', NULL, N'AQM S.A.', 1, NULL, NULL, NULL, 0, NULL, NULL, 5000.0, 0, NULL, 0),
(10, N'Santiago', N'Chile', N'Capta Hydro is a clean energy technology startup that develops and commercialises hydropower technology for generation in artificial canals without the need of falls, that are economically competitive to other distributed generation alternatives and electric utility tariffs. Our BHAG is to install 1 GW of our technology  by 2027.', NULL, N'Capta Hydro', 4, NULL, NULL, NULL, 0, NULL, NULL, 5000.0, 0, NULL, 0),
(2, N'Vi├▒a del Mar', N'Chile', N'Alpaca Samka uses alpaca fibres for our ÔÇ£Slow Fashion ProjectÔÇØ in association with the Aymaras of the Chilean Andes, producing sustainable luxury accessories and garments using traditional Andean methods and British weaving patterns. We are part of the Inward Investment Program and have been recognised by international organisations. ', N'', N'Alpaca Samka SpA', 7, N'', NULL, N'', 0, N'', N'/uploads/enterprise/photo/2/WhatsApp_Image_2017-10-31_at_13.47.22.jpeg', 5000.0, 0, N'', 0),
(11, N'Santiago', N'Chile', N'Our product, Ceptinel Risk Manager, is a real-time business procecess monitoring system that, with the use of applied business rules algorithms, detects and notifies when potentially harmful or beneficial situations are detected.', NULL, N'Ceptinel', 8, NULL, NULL, NULL, 0, NULL, NULL, 5000.0, 0, NULL, 0),
(5, N'Santiago', N'Chile', N'We are Arbol Sabores, a new generation of healthy food that has a positive impact in the environment and society. We want to change the world through the feeding behaviors of the society, giving seeds of urban orchards in ours products and by innovating with biodegradable packing.', NULL, N'├ürbol Sabores', 11, NULL, NULL, NULL, 0, NULL, NULL, 5000.0, 0, NULL, 0),
(6, N'Santiago', N'Chile', N'Our mission is to improve the quality of life of the next 3 billion people living in cities by 2050, providing inspiration, knowledge, and tools to the architects who fill face this challenge.', NULL, N'ArchDaily', 14, NULL, NULL, NULL, 0, NULL, NULL, 5000.0, 0, NULL, 0),
(8, N'SANTIAGO', N'Chile', N'Breal Estate is a Chilean company established in 2013, which, in a strategic alliance with Salesforce.com, initially developed an application for property management. Currently, BReal is an application that incorporates all the functions necessary to manage different processes of the real estate business: property management , sales, leases, common expenses and projects with the best practices in mind. It is delivered as a service (SaaS) and accessed via Internet from any device.', NULL, N'BREAL ESTATE SPA', 17, NULL, NULL, NULL, 0, NULL, NULL, 5000.0, 0, NULL, 0),
(9, N'Santiago', N'Chile', N'We are a intermediary between developers in real estate and small investors who want to get good investment opportunities.  ', N'', N'Capitalizarme.com', 17, N'', NULL, N'', 0, N'', N'/uploads/enterprise/photo/9/cruzeiro.png', 5000.0, 0, N'', 0),
(3, N'Santiago', N'Chile', N' We have one passion: to create value for our customers by analyzing the conversations their customers have with the Contact Center in order to extract valuable and timely information to understand and meet their needs. That┬┤s how AnewLytics was born: a cloud-based analytics service platform that performs 100% automated analysis.', N'', N'AnewLytics SpA', 18, N'', NULL, N'', 0, N'', N'/uploads/enterprise/photo/3/thumb_8016a7d8a952351f3cb4d5f485f43ea1.octet-stream', 5000.0, 0, N'', 0),
(1, N'Santiago', N'Chile', N'Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry', N'', N'AllRide', 21, N'', NULL, N'', 0, N'', N'/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg', 5000.0, 0, N'', 0),
(12, N'Santiago', N'Chile', N'Beyond Productivity: RMES is an asset Performance Management Software that helps capital intensive companies increase productivity through a better use of assets. This technology gets big ammount of data from equipment operation and failures and analyze it to identify improvement opportunities using complex algorithms.', NULL, N'CGS', 21, NULL, NULL, NULL, 0, NULL, NULL, 5000.0, 0, NULL, 0),
(13, N'Santiago', N'Chile', N'ClicEduca - Intesis is a technology development company for education.  Our multidisciplinary team works on RESEARCH AND DEVELOPMENT which has resulted in technological innovations available in educational institutions such as ClicEduca.com cloud and our star product, Mus├¡Glota.  Our know-how philosophy is our base and driving force to develop high educational technology, enhancing teaching and learning processes with innovative solutions for teachers, students, administrators and family today. ', NULL, N'ClicEduca', 22, NULL, NULL, NULL, 0, NULL, NULL, 5000.0, 0, NULL, 0),
(14, N'Santiago', N'Chile', N'ClicEduca - Intesis is a technology development company for education.  Our multidisciplinary team works on RESEARCH AND DEVELOPMENT which has resulted in technological innovations available in educational institutions such as ClicEduca.com cloud and our star product, Mus├¡Glota.  Our know-how philosophy is our base and driving force to develop high educational technology, enhancing teaching and learning processes with innovative solutions for teachers, students, administrators and family today. ', NULL, N'ClicEduca', 22, NULL, NULL, NULL, 0, NULL, NULL, 5000.0, 0, NULL, 0),
(7, N'Providencia', N'Chile', N'Aveeza is an intelligent platform specially developed for managing school transportation, anywhere. From real time ridership status, route optimisation, fleet maintenance to driver management, Aveeza brings world class logistics to school transportation, so the worldÔÇÖs most precious cargo, children, can be monitored in full transparency that the industry finally deserves.', NULL, N'Aveeza', 24, NULL, NULL, NULL, 0, NULL, NULL, 5000.0, 0, NULL, 0);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'City', N'Country', N'Description', N'EmailEnterprise', N'EnterpriseName', N'EnterpriseTypeId', N'Facebook', N'InvestorId', N'Linkedin', N'OwnShares', N'Phone', N'Photo', N'SharePrice', N'Shares', N'Twitter', N'Value') AND [object_id] = OBJECT_ID(N'[Enterprise]'))
    SET IDENTITY_INSERT [Enterprise] OFF;

GO

CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);

GO

CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;

GO

CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);

GO

CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);

GO

CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);

GO

CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);

GO

CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;

GO

CREATE INDEX [IX_Enterprise_EnterpriseTypeId] ON [Enterprise] ([EnterpriseTypeId]);

GO

CREATE INDEX [IX_Enterprise_InvestorId] ON [Enterprise] ([InvestorId]);

GO

CREATE UNIQUE INDEX [IX_Investor_Email] ON [Investor] ([Email]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200720013018_Initial', N'3.1.6');

GO


