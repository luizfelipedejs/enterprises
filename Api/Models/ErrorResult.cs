﻿using Newtonsoft.Json;

namespace Api.Models
{
    public class ErrorResult
    {
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
