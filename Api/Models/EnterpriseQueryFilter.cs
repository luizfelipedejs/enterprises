﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace Api.Models
{
    [BindProperties(SupportsGet = true)]
    public class EnterpriseQueryFilter
    {
        [FromQuery(Name = "enterprise_type")]
        public Nullable<int> EnterpriseType { get; set; }
        [FromQuery(Name = "name")]
        public string Name { get; set; }
    }
}
