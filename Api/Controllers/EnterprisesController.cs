﻿using Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstract;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Authorize]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseService _enterpriseService;

        public EnterprisesController(IEnterpriseService enterpriseService)
        {
            _enterpriseService = enterpriseService;
        }

        [HttpGet]
        public async Task<ActionResult> Enterprises([FromQuery]EnterpriseQueryFilter filter)
        {
            var enterprises = await _enterpriseService
                .GetEnterprises(filter.EnterpriseType, filter.Name);

            return Ok(new
            {
                enterprises = enterprises
            });
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult> Enterprises([FromRoute] int id)
        {
            var enterprise = await _enterpriseService.GetEnterprise(id);

            if (enterprise == null)
                return NotFound(new ErrorResult
                {
                    Status = "404",
                    Error = "Not Found"
                });

            return Ok(new
            {
                enterprise = enterprise,
                success = true
            });
        }
    }
}