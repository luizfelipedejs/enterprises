﻿using Api.Configuration;
using Api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Services.Abstract;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/v{version:apiVersion}/users/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IInvestorService _investorService;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly AppSettings _appSettings;

        public AuthController(IInvestorService investorService,
            SignInManager<IdentityUser> signInManager,
            IOptions<AppSettings> appSettings)
        {
            _investorService = investorService;
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
        }

        [HttpPost]
        [Route("sign_in")]
        public async Task<ActionResult> SignIn([FromBody]SignInModel signIn)
        {
            var investor = await _investorService.GetInvestor(signIn.Email);
            var result = await _signInManager.PasswordSignInAsync(signIn.Email, signIn.Password, false, false);

            if (investor == null || !result.Succeeded)
                return Unauthorized(new
                {
                    success = false,
                    errors = new object[]
                    {
                        "Invalid login credentials. Please try again."
                    }
                });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _appSettings.Issuer,
                Expires = DateTime.UtcNow.AddHours(_appSettings.ExpireHours),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));

            Response.Headers.Add("access-token", token);
            Response.Headers.Add("token-type", "Bearer");
            Response.Headers.Add("uid", signIn.Email);

            return Ok(new
            {
                investor = investor,
                enterprise = investor.Portfolio.Enterprises.FirstOrDefault(),
                success = true
            });
        }
    }
}