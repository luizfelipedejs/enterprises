﻿namespace Api.Configuration
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public int ExpireHours { get; set; }
        public string Issuer { get; set; }
    }
}
