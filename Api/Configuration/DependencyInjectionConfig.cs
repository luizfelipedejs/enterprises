﻿using Data.Repositories;
using Domain.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Services.Abstract;
using Services.Concrete;

namespace Api.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveRepositoriesDependencies(this IServiceCollection services)
        {
            services.AddScoped<IInvestorRepository, InvestorRepository>();
            services.AddScoped<IEnterpriseRepository, EnterpriseRepository>();

            return services;
        }

        public static IServiceCollection ResolveServicesDependencies(this IServiceCollection services)
        {
            services.AddScoped<IInvestorService, InvestorService>();
            services.AddScoped<IEnterpriseService, EnterpriseService>();

            return services;
        }
    }
}
