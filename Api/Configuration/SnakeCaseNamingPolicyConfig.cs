﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;

namespace Api.Configuration
{
    public static class SnakeCaseNamingPolicyConfig
    {
        public static IMvcBuilder SetSnakeCaseNamingPolicy(this IMvcBuilder mvcBuilder)
        {
            mvcBuilder.AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver()
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                };
            });
            return mvcBuilder;
        }
    }
}
