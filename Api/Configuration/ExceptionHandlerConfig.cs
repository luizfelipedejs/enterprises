﻿using Api.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Api.Configuration
{
    public static class ExceptionHandlerConfig
    {
        public static void AddCustomExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    context.Response.StatusCode = 500;
                    context.Response.ContentType = "application/json";

                    await context.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorResult()
                    {
                        Status = context.Response.StatusCode.ToString(),
                        Error = "Oops! Something went wrong!"
                    }));
                });
            });
        }
    }
}
