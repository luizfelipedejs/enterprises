﻿using AutoMapper;
using Domain.Entities;
using Services.Models;
using System.Collections.Generic;
using System.Linq;

namespace Services.MappingProfiles
{
    public class EntityToModelMappingProfile : Profile
    {
        public EntityToModelMappingProfile()
        {
            CreateMap<EnterpriseType, EnterpriseTypeModel>();
            CreateMap<Enterprise, EnterpriseModel>();
            CreateMap<Investor, InvestorModel>()
                .ConvertUsing(src => new InvestorModel()
                {
                    Id = src.Id,
                    InvestorName = src.InvestorName,
                    Email = src.Email,
                    City = src.City,
                    Country = src.Country,
                    Balance = src.Balance,
                    Photo = src.Photo,
                    Portfolio = new PortfolioModel()
                    {
                        Enterprises = src.Enterprises != null ?
                        src.Enterprises.Select(d => new EnterpriseModel()
                        {
                            Id = d.Id,
                            EnterpriseName = d.EnterpriseName,
                            Description = d.Description,
                            EmailEnterprise = d.EmailEnterprise,
                            Facebook = d.Facebook,
                            Twitter = d.Twitter,
                            Linkedin = d.Linkedin,
                            Phone = d.Phone,
                            OwnEnterprise = d.OwnEnterprise,
                            Photo = d.Photo,
                            Value = d.Value,
                            Shares = d.Shares,
                            SharePrice = d.SharePrice,
                            OwnShares = d.OwnShares,
                            City = d.City,
                            Country = d.Country,
                            EnterpriseType = new EnterpriseTypeModel()
                            {
                                Id = d.EnterpriseType.Id,
                                EnterpriseTypeName = d.EnterpriseType.EnterpriseTypeName
                            }
                        }).AsEnumerable() :
                        new List<EnterpriseModel>()
                    },
                    PortfolioValue = src.PortfolioValue,
                    FirstAccess = src.FirstAccess,
                    SuperAngel = src.SuperAngel
                });
        }
    }
}
