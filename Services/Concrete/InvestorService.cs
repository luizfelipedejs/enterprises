﻿using AutoMapper;
using Domain.Repositories;
using Services.Abstract;
using Services.Models;
using System.Threading.Tasks;

namespace Services.Concrete
{
    public class InvestorService : IInvestorService
    {
        private readonly IInvestorRepository _investorRepository;
        private readonly IMapper _mapper;
        public InvestorService(IInvestorRepository investorRepository, IMapper mapper)
        {
            _investorRepository = investorRepository;
            _mapper = mapper;
        }

        public async Task<InvestorModel> GetInvestor(string email)
        {
            var investor = await _investorRepository.GetInvestor(email);

            return investor != null ? _mapper.Map<InvestorModel>(investor) : (InvestorModel)null;
        }
    }
}
