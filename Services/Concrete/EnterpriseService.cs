﻿using AutoMapper;
using Domain.Entities;
using Domain.Repositories;
using Services.Abstract;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Services.Concrete
{
    public class EnterpriseService : IEnterpriseService
    {
        private readonly IEnterpriseRepository _enterpriseRepository;
        private readonly IMapper _mapper;

        public EnterpriseService(IEnterpriseRepository enterpriseRepository, IMapper mapper)
        {
            _enterpriseRepository = enterpriseRepository;
            _mapper = mapper;
        }

        public async Task<EnterpriseModel> GetEnterprise(int id)
        {
            var enterprise = await _enterpriseRepository.GetEnterprise(id);

            return _mapper.Map<Enterprise, EnterpriseModel>(enterprise);
        }


        public async Task<IEnumerable<EnterpriseModel>> GetEnterprises(Nullable<int> enterpriseTypeFilter = null, string nameFilter = null)
        {
            var filters = new List<Expression<Func<Enterprise, bool>>>();

            if (enterpriseTypeFilter.HasValue)
                filters.Add(d => d.EnterpriseTypeId == enterpriseTypeFilter.Value);
            if (!string.IsNullOrEmpty(nameFilter))
                filters.Add(d => d.EnterpriseName.ToLower().Contains(nameFilter.ToLower()));

            var enterprises = await _enterpriseRepository.GetEnterprises(filters.ToArray());

            return _mapper.Map<IEnumerable<EnterpriseModel>>(enterprises);
        }
    }
}
