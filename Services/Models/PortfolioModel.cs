﻿using System.Collections.Generic;
using System.Linq;

namespace Services.Models
{
    public class PortfolioModel
    {
        public int EnterprisesNumber => Enterprises.Count();
        public IEnumerable<EnterpriseModel> Enterprises { get; set; }
    }
}
