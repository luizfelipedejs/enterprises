﻿namespace Services.Models
{
    public class EnterpriseTypeModel
    {
        public int Id { get; set; }
        public string EnterpriseTypeName { get; set; }
    }
}
