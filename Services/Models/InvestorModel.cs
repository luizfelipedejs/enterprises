﻿namespace Services.Models
{
    public class InvestorModel
    {
        public int Id { get; set; }
        public string InvestorName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public decimal Balance { get; set; }
        public string Photo { get; set; }
        public PortfolioModel Portfolio { get; set; }
        public decimal PortfolioValue { get; set; }
        public bool FirstAccess { get; set; }
        public bool SuperAngel { get; set; }
    }
}
