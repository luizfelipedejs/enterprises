﻿using Services.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Abstract
{
    public interface IEnterpriseService
    {
        Task<IEnumerable<EnterpriseModel>> GetEnterprises(Nullable<int> enterpriseTypeFilter = null,
            string nameFilter = null);
        Task<EnterpriseModel> GetEnterprise(int id);
    }
}
