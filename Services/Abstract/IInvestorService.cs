﻿using Services.Models;
using System.Threading.Tasks;

namespace Services.Abstract
{
    public interface IInvestorService
    {
        Task<InvestorModel> GetInvestor(string email);
    }
}
