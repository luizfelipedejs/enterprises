﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class EnterpriseType
    {
        public int Id { get; set; }
        public string EnterpriseTypeName { get; set; }

        public virtual ICollection<Enterprise> Enterprises { get; set; } = new HashSet<Enterprise>();
    }
}
