﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Investor
    {
        public int Id { get; set; }
        public string InvestorName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public decimal Balance { get; set; }
        public string Photo { get; set; }
        public decimal PortfolioValue { get; set; }
        public bool FirstAccess { get; set; }
        public bool SuperAngel { get; set; }

        public virtual ICollection<Enterprise> Enterprises { get; set; } = new HashSet<Enterprise>();
    }
}
