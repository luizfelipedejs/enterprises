﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IEnterpriseRepository
    {
        Task<IEnumerable<Enterprise>> GetEnterprises(params Expression<Func<Enterprise, bool>>[] whereFilters);
        Task<Enterprise> GetEnterprise(int id);
    }
}
