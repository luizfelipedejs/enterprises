﻿using Domain.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IInvestorRepository
    {
        IQueryable<Investor> GetInvestors();
        Task<Investor> GetInvestor(string email);
    }
}
